from flask import Flask, request
app = Flask(__name__)

class Flask_test:
    def home(self, name):
        return 'hello bob from ' + name
    def add(self, a, b):
        return str(a + b)
    def sub(self, a, b):
        return str(a - b)

cc = Flask_test()
@app.route('/', methods=['GET'])
def home():
    return cc.home(request.args.get('name'))

@app.route('/add', methods=['GET'])
def add():
    return cc.add(int(request.args.get('a')),int(request.args.get('b')))

@app.route('/sub', methods=['GET'])
def sub():
    return cc.sub(int(request.args.get('a')),int(request.args.get('b')))

if __name__ == '__main__':  
   app.run('0.0.0.0',port=8039,debug=True)